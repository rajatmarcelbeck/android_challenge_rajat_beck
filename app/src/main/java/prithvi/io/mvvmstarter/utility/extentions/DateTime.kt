package prithvi.io.mvvmstarter.utility.extentions

import android.text.format.DateUtils
import java.text.SimpleDateFormat
import java.util.*

val dateFormatter = SimpleDateFormat("dd MMM yyyy", Locale.getDefault())
val timeFormatter = SimpleDateFormat("KK:mm a", Locale.getDefault())
val dateTimeFormatter = SimpleDateFormat("dd/MM/yy \n KK:mm a", Locale.getDefault())


inline fun Long.dateFormat(formatter: SimpleDateFormat = dateFormatter): String = formatter.format(Date(this))
inline fun Long.timeFormat(formatter: SimpleDateFormat = timeFormatter): String = formatter.format(Date(this))
inline fun Long.dateTimeFormat(formatter: SimpleDateFormat = dateTimeFormatter): String = formatter.format(Date(this))
inline fun Long.relativeTimeFormat(
        now: Long = System.currentTimeMillis(),
        minResolution: Long = 0,
        flag: Int = DateUtils.FORMAT_ABBREV_RELATIVE): String {
    return DateUtils.getRelativeTimeSpanString(this, now, minResolution, flag).toString()
}