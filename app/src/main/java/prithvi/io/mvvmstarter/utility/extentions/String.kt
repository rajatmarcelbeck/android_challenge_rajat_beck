package prithvi.io.mvvmstarter.utility.extentions

import java.text.SimpleDateFormat
import java.util.*

val String.ruppee get()= "\u20B9${this}"

val defaultDateFormat = SimpleDateFormat("dd MMMM yyyy hh:mm", Locale.getDefault())

inline fun String.timestamp(simpleDateFormat: SimpleDateFormat = defaultDateFormat) = simpleDateFormat.parse(this).time
