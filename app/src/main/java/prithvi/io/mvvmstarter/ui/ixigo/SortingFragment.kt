package prithvi.io.mvvmstarter.ui.ixigo

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.AdapterView
import dagger.android.DaggerFragment
import prithvi.io.mvvmstarter.R
import prithvi.io.mvvmstarter.ui.base.BaseDialogFragment
import prithvi.io.mvvmstarter.ui.base.BaseFragment
import android.widget.ArrayAdapter
import kotlinx.android.synthetic.main.fragment_sort.*
import prithvi.io.mvvmstarter.data.models.SortOrder
import prithvi.io.mvvmstarter.utility.extentions.getViewModel
import prithvi.io.mvvmstarter.viewmodel.ViewModelFactory
import javax.inject.Inject


class SortingFragment : BaseDialogFragment() {


    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    val list: List<String> = listOf("Price", "Departure", "Arrival")
    lateinit var dataAdapter: ArrayAdapter<String>
    lateinit var viewModel: SearchFlightViewModel
    var type: SortOrder.Type = SortOrder.Type.PRICE


    companion object {
        fun newInstance(): SortingFragment = SortingFragment()

    }


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.apply {
            setTitle("Filter")
        }

        return dialog
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = activity?.getViewModel(SearchFlightViewModel::class.java, viewModelFactory)!!


    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater?.inflate(R.layout.fragment_sort, container, false)
        dataAdapter = ArrayAdapter<String>(view.context, android.R.layout.simple_spinner_item, list)
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        return view

    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        idSpinner.adapter = dataAdapter

        idSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                type = when (position) {
                    0 -> SortOrder.Type.PRICE
                    1 -> SortOrder.Type.DEPARTURE
                    2 -> SortOrder.Type.ARRIVAL
                    else -> SortOrder.Type.PRICE
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // Code to perform some action when nothing is selected
            }
        }

        btnOk.setOnClickListener {
            dismiss()
            viewModel.sortDetails(SortOrder(type, radioGroup.checkedRadioButtonId == R.id.ascending))
        }

    }


}