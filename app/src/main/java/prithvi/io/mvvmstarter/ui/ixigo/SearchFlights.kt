package prithvi.io.mvvmstarter.ui.ixigo

import android.os.Bundle
import android.os.PersistableBundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import kotlinx.android.synthetic.main.activity_github.*
import kotlinx.android.synthetic.main.activity_search_flights.*
import prithvi.io.mvvmstarter.R
import prithvi.io.mvvmstarter.data.models.Response
import prithvi.io.mvvmstarter.ui.base.BaseActivity
import prithvi.io.mvvmstarter.ui.githubsearch.SearchViewModel
import prithvi.io.mvvmstarter.utility.extentions.getViewModel
import prithvi.io.mvvmstarter.utility.extentions.observe
import prithvi.io.mvvmstarter.viewmodel.ViewModelFactory
import javax.inject.Inject
import android.view.MenuInflater
import android.view.MenuItem
import prithvi.io.mvvmstarter.utility.extentions.toast


class SearchFlights : BaseActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    lateinit var mAdapter: SearchFlightAdapter
    lateinit var viewModel: SearchFlightViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_flights)

        viewModel = getViewModel(SearchFlightViewModel::class.java, viewModelFactory)
        mAdapter = SearchFlightAdapter()
        rvFlights.apply {
            layoutManager = LinearLayoutManager(this@SearchFlights)
            addItemDecoration(object : DividerItemDecoration(context, VERTICAL) {})
            adapter = mAdapter
        }

        viewModel.getFlightDetails()

        observe(viewModel.flightListLiveData) {
            it ?: return@observe
            when (it.status) {
                Response.Status.LOADING -> {
                    parentContainer.showLoading()

                }
                Response.Status.SUCCESS -> {
                    parentContainer.showContent()
                    mAdapter.flightList = it.data ?: listOf()
                }
                Response.Status.ERROR -> {
                    parentContainer.showContent()
                }
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.sort_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            R.id.sort -> {
                SortingFragment.newInstance().show(supportFragmentManager, SortingFragment::class.java.simpleName)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}