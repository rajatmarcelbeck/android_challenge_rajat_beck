package prithvi.io.mvvmstarter.ui.base

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatDialogFragment
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.AndroidSupportInjection
import dagger.android.support.HasSupportFragmentInjector
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.processors.PublishProcessor
import javax.inject.Inject

open class BaseDialogFragment : AppCompatDialogFragment(), HasSupportFragmentInjector, HasDisposableManager {

    @Inject lateinit var fragmentDispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>
    private var compositeDisposable = CompositeDisposable()

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        AndroidSupportInjection.inject(this)
    }

    override fun onDestroyView() {
        dispose()
        super.onDestroyView()
    }

    override fun supportFragmentInjector(): DispatchingAndroidInjector<Fragment> {
        return fragmentDispatchingAndroidInjector
    }

    override fun getCompositeDisposable(): CompositeDisposable {
        if (compositeDisposable.isDisposed)
            compositeDisposable = CompositeDisposable()
        return compositeDisposable
    }

    override fun addDisposable(disposable: Disposable) {
        getCompositeDisposable().add(disposable)
    }

    override fun dispose() {
        getCompositeDisposable().dispose()
    }

}