package prithvi.io.mvvmstarter.ui.ixigo

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import prithvi.io.mvvmstarter.R

import kotlinx.android.synthetic.main.holder_github_user.view.*
import kotlinx.android.synthetic.main.row_flight_details.view.*
import prithvi.io.mvvmstarter.data.models.GithubUser
import prithvi.io.mvvmstarter.data.models.MyFlight
import prithvi.io.mvvmstarter.ui.base.BaseViewHolder
import prithvi.io.mvvmstarter.utility.extentions.*

class SearchFlightAdapter : RecyclerView.Adapter<SearchFlightAdapter.SearchFlightViewHolder>() {


    var flightList: List<MyFlight> = listOf()
        set(value) {
            val diffResult = dispatchListDiff(field, value)
            field = value
            diffResult.dispatchUpdatesTo(this)
        }


    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): SearchFlightViewHolder = SearchFlightViewHolder(p0.inflate(R.layout.row_flight_details))

    override fun getItemCount(): Int = flightList.size

    override fun onBindViewHolder(p0: SearchFlightViewHolder, p1: Int) = p0.bind()

    inner class SearchFlightViewHolder(itemView: View) : BaseViewHolder(itemView) {

        override fun bind() {
            super.bind()
            val rowDetail = flightList[adapterPosition]
            itemView.apply {
                tvSource.text = rowDetail.origin
                tvDestination.text = rowDetail.destination
                tvPrice.text = rowDetail.fare.toString().ruppee
                tvProvider.text = rowDetail.provider
                tvClass.text = rowDetail.classType
                tvStartTime.text = rowDetail.departure?.dateTimeFormat(dateTimeFormatter)
                tvEndTime.text = rowDetail.arrival?.dateTimeFormat(dateTimeFormatter)
            }
        }
    }
}