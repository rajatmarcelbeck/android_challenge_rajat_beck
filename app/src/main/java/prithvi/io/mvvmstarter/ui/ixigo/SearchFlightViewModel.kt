package prithvi.io.mvvmstarter.ui.ixigo

import android.arch.lifecycle.MutableLiveData
import io.reactivex.rxkotlin.subscribeBy
import prithvi.io.mvvmstarter.data.models.MyFlight
import prithvi.io.mvvmstarter.data.models.Response
import prithvi.io.mvvmstarter.data.models.SortOrder
import prithvi.io.mvvmstarter.data.repository.Repository
import prithvi.io.mvvmstarter.ui.base.BaseViewModel
import prithvi.io.mvvmstarter.utility.extentions.fromWorkerToMain
import prithvi.io.mvvmstarter.utility.rx.Scheduler
import javax.inject.Inject

class SearchFlightViewModel @Inject constructor(private val repository: Repository,
                                                private val scheduler: Scheduler) : BaseViewModel() {

    val flightListLiveData: MutableLiveData<Response<List<MyFlight>>> = MutableLiveData()

    fun getFlightDetails() {
        flightListLiveData.value = Response.loading()
        repository.ixigoRepository.getInitialFlightDetails()
                .fromWorkerToMain(scheduler)
                .subscribeBy(
                        onNext = {
                            flightListLiveData.value = Response.success(it)
                        }, onError = {
                    flightListLiveData.value = Response.error(it)
                }
                )

    }

    fun sortDetails(sortOrder: SortOrder) {
        flightListLiveData.value = Response.loading()
        repository.ixigoRepository.sort(sortOrder).fromWorkerToMain(scheduler)
                .subscribeBy(
                        onNext = {
                            flightListLiveData.value = Response.success(it)
                        }, onError = {
                    flightListLiveData.value = Response.error(it)
                }
                )
    }


}