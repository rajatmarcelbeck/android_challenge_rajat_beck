package prithvi.io.mvvmstarter.di.module

import dagger.Module
import dagger.android.ContributesAndroidInjector
import prithvi.io.mvvmstarter.di.FragmentScoped
import prithvi.io.mvvmstarter.ui.base.BaseDialogFragment
import prithvi.io.mvvmstarter.ui.base.BaseFragment
import prithvi.io.mvvmstarter.ui.ixigo.SortingFragment

@Module
abstract class FragmentModule {

    @FragmentScoped
    @ContributesAndroidInjector(modules = [])
    abstract fun baseFragment(): BaseFragment

    @FragmentScoped
    @ContributesAndroidInjector(modules = [])
    abstract fun sortingFragment(): SortingFragment

    @FragmentScoped
    @ContributesAndroidInjector(modules = [])
    abstract fun baseDialogFragment(): BaseDialogFragment

}