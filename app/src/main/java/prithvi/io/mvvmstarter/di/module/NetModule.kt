package prithvi.io.mvvmstarter.di.module

import android.app.Application
import com.squareup.moshi.KotlinJsonAdapterFactory
import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.OkHttpClient
import prithvi.io.mvvmstarter.BuildConfig
import prithvi.io.mvvmstarter.data.api.Api
import prithvi.io.mvvmstarter.data.api.IxigioApi
import prithvi.io.mvvmstarter.utility.Constants
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Named
import javax.inject.Singleton

@Module
class NetModule {

    companion object {
        const val IXIGO: String = "IXIGO"
        const val GITHUB: String = "GITHUB"
    }

    @Provides
    @Singleton
    fun provideOkHttpCache(application: Application): Cache = Cache(application.cacheDir, (10 * 1024 * 1024).toLong())

    @Provides
    @Singleton
    fun provideMoshi(): Moshi = Moshi.Builder().add(KotlinJsonAdapterFactory()).build()

    @Provides
    @Singleton
    @Named(GITHUB)
    fun provideRetrofit(
            moshi: Moshi,
            okHttpClientBuilder: OkHttpClient.Builder
    ): Retrofit {
        return Retrofit.Builder()
                .addConverterFactory(MoshiConverterFactory.create(moshi))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(BuildConfig.API_BASE_URL)
                .client(okHttpClientBuilder.build())
                .build()
    }

    @Provides
    @Singleton
    @Named(IXIGO)
    fun provideIxigoRetrofit(
            moshi: Moshi,
            okHttpClientBuilder: OkHttpClient.Builder
    ): Retrofit {
        return Retrofit.Builder()
                .addConverterFactory(MoshiConverterFactory.create(moshi))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(BuildConfig.IXIGO_URL)
                .client(okHttpClientBuilder.build())
                .build()
    }


    @Provides
    @Singleton
    fun provideApi(@Named(GITHUB) retrofit: Retrofit) = retrofit.create(Api::class.java)

    @Provides
    @Singleton
    fun provideIxigoApi(@Named(IXIGO)retrofit: Retrofit) = retrofit.create(IxigioApi::class.java)


}