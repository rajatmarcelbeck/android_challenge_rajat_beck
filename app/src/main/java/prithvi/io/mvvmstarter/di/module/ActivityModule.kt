package prithvi.io.mvvmstarter.di.module

import dagger.Module
import dagger.android.ContributesAndroidInjector
import prithvi.io.mvvmstarter.ui.githubsearch.SearchActivity
import prithvi.io.mvvmstarter.di.ActivityScoped
import prithvi.io.mvvmstarter.ui.ixigo.SearchFlights

@Module
abstract class ActivityModule {

    @ActivityScoped
    @ContributesAndroidInjector(modules = [])
    abstract fun searchActivity(): SearchActivity

    @ActivityScoped
    @ContributesAndroidInjector(modules = [])
    abstract fun searchFlight(): SearchFlights

}