package prithvi.io.mvvmstarter.data.repository

import io.reactivex.Flowable
import prithvi.io.mvvmstarter.data.api.Api
import prithvi.io.mvvmstarter.data.api.IxigioApi
import prithvi.io.mvvmstarter.data.models.GithubUser
import prithvi.io.mvvmstarter.data.models.IxigoResponse
import prithvi.io.mvvmstarter.data.models.MyFlight
import prithvi.io.mvvmstarter.data.models.SortOrder
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class IxigoRepository @Inject constructor(
        private val ixigioApi: IxigioApi
) {


    private var listFinalFlight: MutableList<MyFlight> = mutableListOf()

    fun getInitialFlightDetails(): Flowable<MutableList<MyFlight>> =
            ixigioApi.getFlightDetails()
                    .map { ixiGoResponse ->
                        val mapAirlines = ixiGoResponse.appendix.airlines
                        val mapAirports = ixiGoResponse.appendix.airports
                        val mapProviders = ixiGoResponse.appendix.providers

                        ixiGoResponse.flights.map { flights ->
                            flights.fares.map { fareDetail ->
                                listFinalFlight.add(
                                        MyFlight(mapAirports[flights.originCode],
                                                mapAirports[flights.destinationCode],
                                                flights.departureTime,
                                                flights.arrivalTime,
                                                fareDetail.fare.toDouble(),
                                                mapProviders[fareDetail.providerId],
                                                mapAirlines[flights.airlineCode],
                                                flights.classX
                                        ))
                            }
                        }
                        listFinalFlight


                    }.toFlowable()


    fun sort(sortOrder: SortOrder): Flowable<MutableList<MyFlight>> {
        if (sortOrder.type.name == SortOrder.Type.PRICE.name) {
            return sortByPrice(sortOrder.order)
        } else if (sortOrder.type.name == SortOrder.Type.ARRIVAL.name) {
            return sortLanding(sortOrder.order)
        }
        return sorttakeOff(sortOrder.order)


    }


    private fun sortByPrice(ascending: Boolean): Flowable<MutableList<MyFlight>> {
        if (ascending)
            listFinalFlight.sortBy { it.fare }
        else
            listFinalFlight.sortByDescending { it.fare }
        return Flowable.just(listFinalFlight).delay(100,TimeUnit.MILLISECONDS)
    }

    private fun sorttakeOff(ascending: Boolean): Flowable<MutableList<MyFlight>> {
        if (ascending)
            listFinalFlight.sortBy { it.departure }
        else
            listFinalFlight.sortByDescending { it.departure }
        return Flowable.just(listFinalFlight).delay(100,TimeUnit.MILLISECONDS)
    }

    private fun sortLanding(ascending: Boolean): Flowable<MutableList<MyFlight>> {
        if (ascending)
            listFinalFlight.sortBy { it.arrival }
        else
            listFinalFlight.sortByDescending { it.arrival }
        return Flowable.just(listFinalFlight).delay(100,TimeUnit.MILLISECONDS)
    }


}