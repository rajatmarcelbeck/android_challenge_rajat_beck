package prithvi.io.mvvmstarter.data.api

import io.reactivex.Single
import prithvi.io.mvvmstarter.data.models.GithubUserResponse
import prithvi.io.mvvmstarter.data.models.IxigoResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface IxigioApi {

    @GET("/v2/5979c6731100001e039edcb3")
    fun getFlightDetails(): Single<IxigoResponse>
}