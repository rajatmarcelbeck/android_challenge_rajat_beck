package prithvi.io.mvvmstarter.data.models

import com.squareup.moshi.Json
import prithvi.io.mvvmstarter.utility.Identifiable

data class Response<out T>(
        val status: Status,
        val data: T? = null,
        val error: Throwable? = null
) {
    enum class Status { LOADING, SUCCESS, ERROR }

    companion object {
        fun <T> loading(): Response<T> = Response(Status.LOADING, null, null)
        fun <T> success(data: T? = null): Response<T> = Response(Status.SUCCESS, data, null)
        fun <T> error(error: Throwable): Response<T> = Response(Status.ERROR, null, error)
    }
}

data class GithubUser(
        @Json(name = "login") val login: String,
        @Json(name = "id") val id: Long,
        @Json(name = "node_id") val nodeId: String,
        @Json(name = "avatar_url") val avatarUrl: String,
        @Json(name = "url") val url: String,
        @Json(name = "html_url") val htmlUrl: String,
        @Json(name = "followers_url") val followersUrl: String,
        @Json(name = "following_url") val followingUrl: String,
        @Json(name = "gists_url") val gistsUrl: String,
        @Json(name = "starred_url") val starredUrl: String,
        @Json(name = "subscriptions_url") val subscriptionsUrl: String,
        @Json(name = "organizations_url") val organizationsUrl: String,
        @Json(name = "repos_url") val reposUrl: String,
        @Json(name = "events_url") val eventsUrl: String,
        @Json(name = "received_events_url") val receivedEventsUrl: String,
        @Json(name = "type") val type: String,
        @Json(name = "score") val score: Float
) : Identifiable {
    override val identifier: Long get() = id
}

data class EditTextFlow(
        val query: String,
        val type: Type
) {
    enum class Type { BEFORE, AFTER, ON }

}

data class IxigoResponse(
        val appendix: Appendix,
        val flights: List<Flight>
) {
    data class Appendix(
            val airlines: Map<String, String>,
            val airports: Map<String, String>,
            val providers: Map<Int, String>
    )

    data class Flight(
            val airlineCode: String, // 6E
            val arrivalTime: Long, // 1396620000000
            @Json(name = "class")
            val classX: String, // Economy
            val departureTime: Long, // 1396612800000
            val destinationCode: String, // BOM
            val fares: List<Fare>,
            val originCode: String // DEL
    ) {
        data class Fare(
                val fare: Int, // 5500
                val providerId: Int // 3
        )
    }
}

data class MyFlight(
        val origin: String?,
        val destination: String?,
        val departure: Long?,
        val arrival: Long?,
        val fare: Double?,
        val provider: String?,
        val airlineName: String?,
        val classType: String?
) : Identifiable {
    override val identifier: Long
        get() = hashCode().toLong()
}


data class SortOrder(
        val type: Type,
        val order: Boolean
) {
    enum class Type { PRICE, ARRIVAL, DEPARTURE }
}



